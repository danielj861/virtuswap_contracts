// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./Types256.sol";
import "./ERC20/IERC20.sol";
import "./ERC20/ERC20.sol";
import "./vPoolCalculations.sol";

contract vPoolsManager {
    uint256 public constant MINIMUM_LIQUIDITY = 10**3;

    mapping(string => uint256) poolKeys;

    // event Debug(string message, int256 value);

    // event UDebug(string message, uint256 value);

    // event ADebug(string message, address value);

    int256 imbalance_tolerance_base = 0.01 ether;

    Pool[] rPools;
    mapping(uint256 => ReserveBalance[]) reserves;
    address[] _tokens;

    address owner;

    constructor() {
        owner = msg.sender;
        rPools.push(); //push first empty pool to allocate 0 index
    }

    function getRPoolIndex(address tokenA, address tokenB)
        internal
        view
        returns (uint256)
    {
        string memory key = vPoolCalculations.appendAddresses(tokenA, tokenB);
        return poolKeys[key];
    }

    //temporary function. will be refactored
    function getRPools() public view returns (PoolVM[] memory) {
        PoolVM[] memory temp = new PoolVM[](rPools.length);

        // int256[] memory reserveRatio = _calculateReserveRatio();
        // int256[] memory belowReserve = _calculateBelowThreshold();

        for (uint256 i = 0; i < rPools.length; i++) {
            temp[i].tokenA = rPools[i].tokenA;
            temp[i].tokenB = rPools[i].tokenB;
            temp[i].fee = rPools[i].fee;
            temp[i].tokenABalance = rPools[i].tokenABalance;
            temp[i].tokenBBalance = rPools[i].tokenBBalance;
        }
        return temp;
    }

    function _calculateVirtualPool(uint256[] memory ks, uint256[] memory js)
        public
        view
        returns (VirtualPool memory)
    {
        VirtualPool memory vPool = vPoolCalculations._calculateVirtualPool(
            rPools,
            ks,
            js
        );

        //get rPool
        uint256 rPoolIndex = getRPoolIndex(vPool.tokenA, vPool.tokenB);
        vPool.rPoolIndex = rPoolIndex;
        return vPool;
    }

    function getTotalPool(uint256[] memory ks, uint256[] memory js)
        public
        view
        returns (VirtualPool memory)
    {
        VirtualPool memory vPool = _calculateVirtualPool(ks, js);

        VirtualPool memory tPool = vPoolCalculations.getTotalPool(
            rPools,
            vPool
        );

        return tPool;
    }

    function quote(
        uint256[] memory ks,
        uint256[] memory js,
        int256 amount
    ) public view returns (int256) {
        VirtualPool memory tPool = getTotalPool(ks, js);
        return vPoolCalculations.quote(rPools, tPool, amount);
    }

    function swap(
        uint256[] memory ks,
        uint256[] memory js,
        int256 amount
    ) public {
        // require(
        //     IERC20(tPool.tokenA).transferFrom(
        //         msg.sender,
        //         address(this),
        //         uint256(amount)
        //     ),
        //     "Failed to transfer token A to contract"
        // );

        VirtualPool memory tPool = getTotalPool(ks, js);

        require(
            IERC20(tPool.tokenA).transferFrom(
                0x57161f5aFbfBacC69aad1905EeA0DE87ebC6853f,
                // address(msg.sender),
                address(this),
                uint256(amount)
            ),
            "Failed to transfer token A to contract"
        );

        int256 outBalance = vPoolCalculations.quote(rPools, tPool, amount);

        VirtualPool[] memory lagR = new VirtualPool[](rPools.length);

        // %save current values

        //lag_T(buy_currency,sell_currency,buy_currency)=T(buy_currency,sell_currency,buy_currency);
        int256 lagTTokenABalance = tPool.tokenABalance;

        //lag_T(buy_currency,sell_currency,sell_currency)=T(buy_currency,sell_currency,sell_currency);
        int256 lagTTokenBBalance = tPool.tokenBBalance;

        //%substract amount and add fees to amount_in
        //T(buy_currency,sell_currency,buy_currency)=lag_T(buy_currency,sell_currency,buy_currency)-Buy*(1-fee_T(buy_currency,sell_currency)); ****
        tPool.tokenABalance =
            lagTTokenABalance -
            (amount - ((tPool.fee * amount) / 1 ether));

        // T(buy_currency,sell_currency,sell_currency)=lag_T(buy_currency,sell_currency,buy_currency)*lag_T(buy_currency,sell_currency,sell_currency)/(lag_T(buy_currency,sell_currency,buy_currency)-Buy); // %calculate amount_out
        tPool.tokenBBalance = tPool.tokenBBalance =
            (lagTTokenABalance * lagTTokenBBalance) /
            (lagTTokenABalance - amount);

        // for k=1:number_currencies
        for (uint256 i = 0; i < ks.length; i++) {
            //lag_R(buy_currency,k,buy_currency)=R(buy_currency,k,buy_currency);
            lagR[ks[i]].tokenABalance = rPools[ks[i]].tokenABalance;

            //lag_R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency);
            lagR[ks[i]].tokenBBalance = rPools[ks[i]].tokenBBalance;
        }

        // %take buy_currency proportional from real and virtual pool
        /*  R(buy_currency,sell_currency,buy_currency)=
                   lag_R(buy_currency,sell_currency,buy_currency) *
                   T(buy_currency,sell_currency,buy_currency)/
                   lag_T(buy_currency,sell_currency,buy_currency); */

        //lag_R(buy_currency,k,buy_currency)=R(buy_currency,k,buy_currency);
        lagR[tPool.rPoolIndex].tokenABalance = rPools[tPool.rPoolIndex]
            .tokenABalance;

        //lag_R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency);
        lagR[tPool.rPoolIndex].tokenBBalance = rPools[tPool.rPoolIndex]
            .tokenBBalance;

        // %take sell_currency proportional from real and virtual pool

        if (tPool.rPoolIndex > 0) {
            /* R(buy_currency,sell_currency,sell_currency)=
        lag_R(buy_currency,sell_currency,sell_currency)*
        T(buy_currency,sell_currency,sell_currency)/
        lag_T(buy_currency,sell_currency,sell_currency);*/
            rPools[tPool.rPoolIndex].tokenABalance =
                (lagR[tPool.rPoolIndex].tokenABalance * tPool.tokenABalance) /
                lagTTokenABalance;

            rPools[tPool.rPoolIndex].tokenBBalance =
                (lagR[tPool.rPoolIndex].tokenBBalance * tPool.tokenBBalance) /
                lagTTokenBBalance;

            // %fill reverse
            // R(sell_currency,buy_currency,buy_currency)=R(buy_currency,sell_currency,buy_currency);
            rPools[rPools[tPool.rPoolIndex].reversePoolIndex]
                .tokenBBalance = rPools[tPool.rPoolIndex].tokenABalance;

            // R(sell_currency,buy_currency,sell_currency)=R(buy_currency,sell_currency,sell_currency);
            rPools[rPools[tPool.rPoolIndex].reversePoolIndex]
                .tokenABalance = rPools[tPool.rPoolIndex].tokenBBalance;
        }

        //% Updating of non-native pools that contribute to BC virtual pool;
        for (uint256 i = 0; i < ks.length; i++) {
            //sum lagR tokenA balance

            /*R(buy_currency,k,buy_currency)=
            //R(buy_currency,k,buy_currency)+
            ((T(buy_currency,sell_currency,buy_currency)-lag_T(buy_currency,sell_currency,buy_currency))-
            (R(buy_currency,sell_currency,buy_currency)-lag_R(buy_currency,sell_currency,buy_currency)))*
            lag_R(buy_currency,k,buy_currency)/
            (sum(lag_R(buy_currency,1:number_currencies,buy_currency))
            -lag_R(buy_currency,sell_currency,buy_currency));

*/
            rPools[ks[i]].tokenABalance =
                rPools[ks[i]].tokenABalance +
                (((tPool.tokenABalance - lagTTokenABalance) -
                    (rPools[tPool.rPoolIndex].tokenABalance -
                        lagR[tPool.rPoolIndex].tokenABalance)) *
                    lagR[ks[i]].tokenABalance) /
                (vPoolCalculations.sumVirtualPoolsArray(lagR) -
                    lagR[tPool.rPoolIndex].tokenABalance);

            //fill reverse pool
            //R(k,buy_currency,buy_currency)=R(buy_currency,k,buy_currency);
            rPools[rPools[ks[i]].reversePoolIndex].tokenBBalance = rPools[ks[i]]
                .tokenABalance;
        }

        for (uint256 i = 0; i < js.length; i++) {
            //sum lagR tokenA balance

            /*R(buy_currency,k,buy_currency)=
            //R(buy_currency,k,buy_currency)+
            ((T(buy_currency,sell_currency,buy_currency)-lag_T(buy_currency,sell_currency,buy_currency))-
            (R(buy_currency,sell_currency,buy_currency)-lag_R(buy_currency,sell_currency,buy_currency)))*
            lag_R(buy_currency,k,buy_currency)/
            (sum(lag_R(buy_currency,1:number_currencies,buy_currency))
            -lag_R(buy_currency,sell_currency,buy_currency));

*/
            rPools[js[i]].tokenABalance =
                rPools[js[i]].tokenABalance +
                (((tPool.tokenABalance - lagTTokenABalance) -
                    (rPools[tPool.rPoolIndex].tokenABalance -
                        lagR[tPool.rPoolIndex].tokenABalance)) *
                    lagR[js[i]].tokenABalance) /
                (vPoolCalculations.sumVirtualPoolsArray(lagR) -
                    lagR[tPool.rPoolIndex].tokenABalance);

            //fill reverse pool
            //R(k,buy_currency,buy_currency)=R(buy_currency,k,buy_currency);
            rPools[rPools[js[i]].reversePoolIndex].tokenBBalance = rPools[js[i]]
                .tokenABalance;
        }

        // % Updating reserves of real pools and all the subsequent calculations;
        // i=buy_currency;
        // for k=1:number_currencies
        //     if (k~=buy_currency & k~=sell_currency)
        //         R(buy_currency,k,sell_currency)=R(buy_currency,k,sell_currency)+((T(buy_currency,sell_currency,sell_currency)-lag_T(buy_currency,sell_currency,sell_currency))-(R(buy_currency,sell_currency,sell_currency)-lag_R(buy_currency,sell_currency,sell_currency)))*lag_R(buy_currency,k,buy_currency)/(sum(lag_R(buy_currency,1:number_currencies,buy_currency))-lag_R(buy_currency,sell_currency,buy_currency));
        //         R(k,buy_currency,sell_currency)=R(buy_currency,k,sell_currency);
        //     end;
        // end;
        for (uint256 i = 0; i < ks.length; i++) {
            //sum lagR tokenA balance

            /*R(buy_currency,k,sell_currency)=
            R(buy_currency,k,sell_currency)+
            ((T(buy_currency,sell_currency,sell_currency)-lag_T(buy_currency,sell_currency,sell_currency))-
            (R(buy_currency,sell_currency,sell_currency)-lag_R(buy_currency,sell_currency,sell_currency)))*
            lag_R(buy_currency,k,buy_currency)/(sum(lag_R(buy_currency,1:number_currencies,buy_currency))-
            lag_R(buy_currency,sell_currency,buy_currency));*/

            rPools[ks[i]].reserves[rPools[ks[i]].tokenB].reserveBalance =
                rPools[ks[i]].reserves[rPools[ks[i]].tokenB].reserveBalance +
                (((tPool.tokenBBalance - lagTTokenBBalance) -
                    (rPools[tPool.rPoolIndex].tokenBBalance -
                        lagR[tPool.rPoolIndex].tokenBBalance)) *
                    lagR[ks[i]].tokenABalance) /
                (vPoolCalculations.sumVirtualPoolsArray(lagR) -
                    lagR[tPool.rPoolIndex].tokenABalance);

            //fill reverse pool
            rPools[rPools[ks[i]].reversePoolIndex]
                .reserves[rPools[ks[i]].tokenB]
                .reserveBalance = rPools[ks[i]]
                .reserves[rPools[ks[i]].tokenB]
                .reserveBalance;
        }

        // emit ADebug("2 msg.sender", msg.sender);
        // emit ADebug("2 address(this)", address(this));
        // emit UDebug("2 amount", uint256(outBalance));

        require(
            IERC20(tPool.tokenB).transfer(
                0x57161f5aFbfBacC69aad1905EeA0DE87ebC6853f,
                uint256(outBalance)
            ),
            "Failed to transfer out amount to user"
        );

        //calculate below threshold for rPool
    }

    function calculateUniswapIndirect(
        uint256[] memory ks,
        uint256[] memory js,
        int256 amount
    ) public view returns (int256) {
        return
            vPoolCalculations.calculateUniswapIndirect(rPools, ks, js, amount);
    }

    function calculateVswapCost(
        uint256[] memory ks,
        uint256[] memory js,
        int256 amount
    ) public view returns (int256) {
        VirtualPool memory tPool = getTotalPool(ks, js);
        return vPoolCalculations.calculateVswapCost(tPool, amount);
    }

    function createPool(address tokenA, address tokenB)
        internal
        returns (uint256)
    {
        //check if pool exist
        string memory key = vPoolCalculations.appendAddresses(tokenA, tokenB);
        string memory reverseKey = vPoolCalculations.appendAddresses(
            tokenB,
            tokenA
        );

        require(poolKeys[key] == 0, "Pool already exist");
        require(poolKeys[reverseKey] == 0, "Pool already exist");

        rPools.push();

        uint256 poolIndex = rPools.length - 1;
        poolKeys[key] = poolIndex;

        Pool storage newPool = rPools[poolIndex];
        newPool.id = poolIndex;
        newPool.tokenA = tokenA;
        newPool.tokenB = tokenB;
        newPool.belowReserve = 1;
        newPool.fee = 0.002 ether;
        newPool.maxReserveRatio = 0.02 ether;

        rPools.push();

        uint256 reversePoolIndex = rPools.length - 1;
        poolKeys[reverseKey] = reversePoolIndex;

        Pool storage newReversePool = rPools[reversePoolIndex];
        newReversePool.id = reversePoolIndex;
        newReversePool.tokenA = tokenB;
        newReversePool.tokenB = tokenA;
        newReversePool.belowReserve = 1;
        newReversePool.fee = 0.002 ether;
        newReversePool.maxReserveRatio = 0.02 ether;

        newPool.reversePoolIndex = reversePoolIndex;
        newReversePool.reversePoolIndex = poolIndex;

        if (!vPoolCalculations.tokenExist(_tokens, tokenA)) {
            _tokens.push(tokenA);
        }

        if (!vPoolCalculations.tokenExist(_tokens, tokenB)) {
            _tokens.push(tokenB);
        }

        //create LP token
        string memory name = string(abi.encodePacked("VSWPLP", rPools.length));

        ERC20 LPToken = new ERC20(name, "VSWPLP", 1000000 ether);
        newPool.LPToken = address(LPToken);
        newReversePool.LPToken = address(LPToken);

        //mint first tokens to blackhole
        //    LPToken._mint(address(0), MINIMUM_LIQUIDITY);

        return poolIndex;
    }

    // function emptyLiquidity(uint256 poolIndex) public {
    //     require(msg.sender == owner, "Only owner"); //set modifier
    //     require(poolIndex > 0, "No pool specified");

    //     require(
    //         IERC20(rPools[poolIndex].tokenA).transfer(
    //             owner,
    //             uint256(rPools[poolIndex].tokenABalance)
    //         ),
    //         "Failed to send token A"
    //     );
    //     rPools[poolIndex].tokenABalance = 0;

    //     require(
    //         IERC20(rPools[poolIndex].tokenB).transfer(
    //             owner,
    //             uint256(rPools[poolIndex].tokenBBalance)
    //         ),
    //         "Failed to send token B"
    //     );

    //     rPools[poolIndex].tokenBBalance = 0;
    // }

    function depositLiquidity(
        uint256 poolIndex,
        address tokenA,
        address tokenB,
        uint256 tokenAAmount,
        uint256 tokenBAmount
    ) public {
        IERC20 tokenAInstance = IERC20(tokenA);
        IERC20 tokenBInstance = IERC20(tokenB);

        if (poolIndex == 0) {
            poolIndex = createPool(tokenA, tokenB);
        }

        require(poolIndex > 0, "Error creating pool");

        require(
            tokenAInstance.transferFrom(
                msg.sender,
                address(this),
                tokenAAmount
            ),
            "Could not transfer token A"
        );
        require(
            tokenBInstance.transferFrom(
                msg.sender,
                address(this),
                tokenBAmount
            ),
            "Could not transfer token B"
        );

        rPools[poolIndex].tokenABalance =
            rPools[poolIndex].tokenABalance +
            int256(tokenAAmount);

        rPools[poolIndex].tokenBBalance =
            rPools[poolIndex].tokenBBalance +
            int256(tokenBAmount);

        //reverse pools
        rPools[poolIndex + 1].tokenABalance = rPools[poolIndex].tokenBBalance;
        rPools[poolIndex + 1].tokenBBalance = rPools[poolIndex].tokenABalance;

        /* t(add_currency_base,add_currency_quote,LP)=
lag_t(add_currency_base,add_currency_quote,LP)+Add*
sum(lag_t(add_currency_base,add_currency_quote,:))/
(lag_R(add_currency_base,add_currency_quote,add_currency_base)*
(1+reserve_ratio(add_currency_base,add_currency_quote)*
(1+Add/lag_R(add_currency_base,add_currency_quote,add_currency_base))));
*/

        // uint lagT

        // //issue LP tokens
        // ERC20(rPools[poolIndex].LPToken)._mint(msg.sender, tokenAAmount);
    }

    function getTokens() public view returns (address[] memory) {
        return _tokens;
    }

    function getPoolsCount() public view returns (uint256) {
        return rPools.length;
    }
}
